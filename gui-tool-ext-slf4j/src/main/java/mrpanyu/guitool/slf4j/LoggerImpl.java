package mrpanyu.guitool.slf4j;

import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.helpers.MessageFormatter;

import mrpanyu.guitool.base.util.Log;

public class LoggerImpl extends MarkerIgnoringBase {

	private static final long serialVersionUID = 1L;

	private String name;

	LoggerImpl(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public boolean isTraceEnabled() {
		return false;
	}

	@Override
	public void trace(String msg) {
	}

	@Override
	public void trace(String format, Object arg) {
	}

	@Override
	public void trace(String format, Object arg1, Object arg2) {
	}

	@Override
	public void trace(String format, Object... arguments) {
	}

	@Override
	public void trace(String msg, Throwable t) {
	}

	@Override
	public boolean isDebugEnabled() {
		return Log.isDebugEnabled();
	}

	private String fmt(String format, Object arg) {
		return MessageFormatter.format(format, arg).getMessage();
	}

	private String fmt(String format, Object arg1, Object arg2) {
		return MessageFormatter.format(format, arg1, arg2).getMessage();
	}

	private String fmt(String format, Object... args) {
		return MessageFormatter.arrayFormat(format, args).getMessage();
	}

	@Override
	public void debug(String msg) {
		Log.debug(msg);
	}

	@Override
	public void debug(String format, Object arg) {
		Log.debug(fmt(format, arg));
	}

	@Override
	public void debug(String format, Object arg1, Object arg2) {
		Log.debug(fmt(format, arg1, arg2));
	}

	@Override
	public void debug(String format, Object... arguments) {
		Log.debug(fmt(format, arguments));
	}

	@Override
	public void debug(String msg, Throwable t) {
		Log.debug(fmt(msg, t));
	}

	@Override
	public boolean isInfoEnabled() {
		return true;
	}

	@Override
	public void info(String msg) {
		Log.info(msg);
	}

	@Override
	public void info(String format, Object arg) {
		Log.info(fmt(format, arg));
	}

	@Override
	public void info(String format, Object arg1, Object arg2) {
		Log.info(fmt(format, arg1, arg2));
	}

	@Override
	public void info(String format, Object... arguments) {
		Log.info(fmt(format, arguments));
	}

	@Override
	public void info(String msg, Throwable t) {
		Log.info(fmt(msg, t));
	}

	@Override
	public boolean isWarnEnabled() {
		return true;
	}

	@Override
	public void warn(String msg) {
		Log.warn(msg);
	}

	@Override
	public void warn(String format, Object arg) {
		Log.warn(fmt(format, arg));
	}

	@Override
	public void warn(String format, Object... arguments) {
		Log.warn(fmt(format, arguments));
	}

	@Override
	public void warn(String format, Object arg1, Object arg2) {
		Log.warn(fmt(format, arg1, arg2));
	}

	@Override
	public void warn(String msg, Throwable t) {
		Log.warn(fmt(msg, t));
	}

	@Override
	public boolean isErrorEnabled() {
		return true;
	}

	@Override
	public void error(String msg) {
		Log.error(msg);
	}

	@Override
	public void error(String format, Object arg) {
		Log.error(fmt(format, arg));
	}

	@Override
	public void error(String format, Object arg1, Object arg2) {
		Log.error(fmt(format, arg1, arg2));
	}

	@Override
	public void error(String format, Object... arguments) {
		Log.error(fmt(format, arguments));
	}

	@Override
	public void error(String msg, Throwable t) {
		Log.error(fmt(msg, t));
	}

}
