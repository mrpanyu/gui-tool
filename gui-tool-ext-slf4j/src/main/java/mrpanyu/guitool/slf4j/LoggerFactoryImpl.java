package mrpanyu.guitool.slf4j;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

public class LoggerFactoryImpl implements ILoggerFactory {

	@Override
	public Logger getLogger(String name) {
		return new LoggerImpl(name);
	}

}
