package org.slf4j.impl;

import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;

import mrpanyu.guitool.slf4j.LoggerFactoryImpl;

public class StaticLoggerBinder implements LoggerFactoryBinder {

	private static StaticLoggerBinder SINGLETON = new StaticLoggerBinder();
	private static LoggerFactoryImpl FACTORY = new LoggerFactoryImpl();

	@Override
	public ILoggerFactory getLoggerFactory() {
		return FACTORY;
	}

	@Override
	public String getLoggerFactoryClassStr() {
		return LoggerFactoryImpl.class.getName();
	}

	public static StaticLoggerBinder getSingleton() {
		return SINGLETON;
	}
}
