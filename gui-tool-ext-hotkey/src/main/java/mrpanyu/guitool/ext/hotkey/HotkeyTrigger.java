package mrpanyu.guitool.ext.hotkey;

import com.github.kwhat.jnativehook.keyboard.NativeKeyEvent;

/**
 * 全局热键触发条件
 * <p>
 * keyCode对应<code>NativeKeyEvent.VC_ESCAPE</code>这样的keyCode值
 * <p>
 * ctrl/shift/alt对应相关的键是否处于按下状态
 */
public class HotkeyTrigger {

	private int keyCode = 0;
	private boolean ctrl;
	private boolean shift;
	private boolean alt;

	public HotkeyTrigger() {
	}

	public HotkeyTrigger(int keyCode, boolean ctrl, boolean shift, boolean alt) {
		super();
		this.keyCode = keyCode;
		this.ctrl = ctrl;
		this.shift = shift;
		this.alt = alt;
	}

	public int getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}

	public boolean isCtrl() {
		return ctrl;
	}

	public void setCtrl(boolean ctrl) {
		this.ctrl = ctrl;
	}

	public boolean isShift() {
		return shift;
	}

	public void setShift(boolean shift) {
		this.shift = shift;
	}

	public boolean isAlt() {
		return alt;
	}

	public void setAlt(boolean alt) {
		this.alt = alt;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (ctrl) {
			sb.append("CTRL+");
		}
		if (shift) {
			sb.append("SHIFT+");
		}
		if (alt) {
			sb.append("ALT+");
		}
		sb.append(NativeKeyEvent.getKeyText(keyCode));
		return sb.toString();
	}

}
