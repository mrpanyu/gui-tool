# GUI-TOOL

This is a simple framework for quickly building java swing based gui tools.

Useful when you need to create some simple gui tool for colleages but do not know(or care) how to write swing code.

## Usage

1. `mvn install` the gui-tool-base project.
2. Make your own maven project, include the gui-tool-base as dependency. Or you can copy gui-tool-example project as base.
3. See example in gui-tool-example project for how to use the api.
