package mrpanyu.guitool.example;

import java.util.Arrays;

import mrpanyu.guitool.base.GuiToolRunner;

/**
 * Main class for your tool
 * 
 * @author panyu
 *
 */
public class ExampleMain {

	public static void main(String[] args) {
		// Here in main function we calls GuiToolRunner.run() to start the tool window.
		GuiToolRunner.run(Arrays.asList(DigestTool.class, LineCounter.class), args);

		// If there is only one annotated class, the left menu will not show.
		// GuiToolRunner.run(DigestTool.class, args);

		// You can also use a base package to start all tools under all its sub-packages
		// GuiToolRunner.run("mrpanyu.guitool", args);
	}

}
