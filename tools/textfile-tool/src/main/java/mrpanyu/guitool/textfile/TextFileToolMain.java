package mrpanyu.guitool.textfile;

import java.util.ArrayList;
import java.util.List;

import mrpanyu.guitool.base.GuiToolRunner;

public class TextFileToolMain {

	public static void main(String[] args) {
		List<Class<?>> tools = new ArrayList<>();

		tools.add(Base64Tool.class);
		tools.add(EscapeTool.class);
		tools.add(ExtractByFileList.class);
		tools.add(SplitJavaAndResourceFileTool.class);

		GuiToolRunner.run(tools, args);
	}

}
