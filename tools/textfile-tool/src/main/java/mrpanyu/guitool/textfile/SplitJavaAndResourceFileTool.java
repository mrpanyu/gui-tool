package mrpanyu.guitool.textfile;

import java.io.File;

import cn.hutool.core.io.FileUtil;
import mrpanyu.guitool.base.annotation.Action;
import mrpanyu.guitool.base.annotation.Parameter;
import mrpanyu.guitool.base.annotation.ToolModel;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;

@ToolModel(displayName = "Maven工程: 分离java/resources文件", description = "Maven工程目录结构，自动将误放在java目录的资源文件放到resources目录")
public class SplitJavaAndResourceFileTool {

	@Parameter(displayName = "工程目录", description = "Maven工程目录", type = ParameterType.DIRECTORY, order = 1)
	private File projectFolder;

	@Action(displayName = "分离", order = 1)
	public void split(Tool tool) throws Exception {
		tool.clearMessages();
		if (projectFolder == null || !projectFolder.isDirectory()) {
			tool.errorMessage("工程目录不存在: " + projectFolder);
			return;
		}
		File javaDir = new File(projectFolder.getAbsolutePath() + "/src/main/java");
		if (!javaDir.isDirectory()) {
			tool.errorMessage("工程目录中没有src/main/java目录");
			return;
		}
		File resDir = new File(projectFolder.getAbsolutePath() + "/src/main/resources");
		split(tool, javaDir, resDir);
		tool.infoMessage("全部完成");
	}

	private void split(Tool tool, File javaDir, File resDir) throws Exception {
		for (File f : javaDir.listFiles()) {
			if (f.isDirectory()) {
				String name = f.getName();
				File javaDirSub = new File(javaDir, name);
				File resDirSub = new File(resDir, name);
				split(tool, javaDirSub, resDirSub);
			} else {
				String name = f.getName();
				if (!name.endsWith(".java")) {
					tool.infoMessage("移动" + f.getAbsolutePath() + "至" + resDir);
					resDir.mkdirs();
					FileUtil.move(f, resDir, true);
				}
			}
		}
	}

}
