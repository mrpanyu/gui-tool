package mrpanyu.guitool.textfile;

import java.nio.charset.Charset;

import cn.hutool.core.net.URLDecoder;
import cn.hutool.core.net.URLEncodeUtil;
import cn.hutool.core.util.EscapeUtil;
import mrpanyu.guitool.base.annotation.Action;
import mrpanyu.guitool.base.annotation.Parameter;
import mrpanyu.guitool.base.annotation.ToolModel;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;

@ToolModel(displayName = "URL/XML转义", description = "", enableProfiles = false)
public class EscapeTool {

	@Parameter(displayName = "转义方式", type = ParameterType.SELECT, options = { "URLEncode", "URLEncodeAll", "URLDecode",
			"XMLEscape", "XMLUnescape" }, order = 1)
	private String type;

	@Parameter(displayName = "输入", type = ParameterType.MULTILINE_TEXT)
	private String textValue = "";

	@Action(displayName = "转义", order = 1)
	public void escape(Tool tool) {
		tool.clearMessages();
		String output = null;
		if ("URLEncode".equals(type)) {
			output = URLEncodeUtil.encode(textValue);
		} else if ("URLEncodeAll".equals(type)) {
			output = URLEncodeUtil.encodeAll(textValue);
		} else if ("URLDecode".equals(type)) {
			output = URLDecoder.decode(textValue, Charset.forName("UTF-8"));
		} else if ("XMLEscape".equals(type)) {
			output = EscapeUtil.escapeXml(textValue);
		} else if ("XMLUnescape".equals(type)) {
			output = EscapeUtil.unescapeXml(textValue);
		}
		tool.infoMessage(output);
	}

}
