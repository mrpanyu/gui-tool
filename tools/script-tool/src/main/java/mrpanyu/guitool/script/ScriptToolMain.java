package mrpanyu.guitool.script;

import mrpanyu.guitool.base.GuiToolRunner;

public class ScriptToolMain {

	public static void main(String[] args) {
		GuiToolRunner.run(ScriptTool.class, args);
	}

}
