package mrpanyu.guitool.bizno;

import java.util.UUID;

import mrpanyu.guitool.base.annotation.Action;
import mrpanyu.guitool.base.annotation.Parameter;
import mrpanyu.guitool.base.annotation.ToolModel;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;

@ToolModel(displayName = "号码生成", description = "各种随机号码", enableProfiles = false)
public class RandomNoGenerator {

	@Parameter(displayName = "类型", type = ParameterType.SELECT, options = { "UUID" }, order = 0)
	private String type = "UUID";

	@Parameter(displayName = "数量", order = 1)
	private int count = 1;

	@Action(displayName = "生成", order = 0)
	public void generate(Tool tool) {
		tool.clearMessages();
		for (int i = 0; i < count; i++) {
			generateOne(tool);
		}
	}

	private void generateOne(Tool tool) {
		String generated = null;
		switch (type) {
		case "UUID":
			generated = generateUUID();
		}
		tool.infoMessage(generated);
	}

	private String generateUUID() {
		return UUID.randomUUID().toString();
	}

}
