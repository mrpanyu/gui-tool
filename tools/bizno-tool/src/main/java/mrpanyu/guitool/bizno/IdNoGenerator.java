package mrpanyu.guitool.bizno;

import mrpanyu.guitool.base.annotation.Action;
import mrpanyu.guitool.base.annotation.Parameter;
import mrpanyu.guitool.base.annotation.ToolModel;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;

@ToolModel(displayName = "身份证修正", description = "根据身份证号规则修正最后一位", enableProfiles = false)
public class IdNoGenerator {

	@Parameter(displayName = "身份证号", type = ParameterType.TEXT, description = "18位身份证号，最后一位可以随便填")
	private String idNo;

	@Action(displayName = "修正", order = 0)
	public void correct(Tool tool) {
		if (idNo.length() < 18) {
			tool.errorMessage("请输入18位身份证号");
			return;
		}
		String correctIdNo = correctIdNumber(idNo);
		tool.infoMessage(correctIdNo);
	}

	@Action(displayName = "清空", order = 1)
	public void clear(Tool tool) {
		tool.clearMessages();
	}

	public static String correctIdNumber(String idNumber) {
		String bit = "";
		int sum = Integer.parseInt(idNumber.substring(0, 1)) * 7 + Integer.parseInt(idNumber.substring(1, 2)) * 9
				+ Integer.parseInt(idNumber.substring(2, 3)) * 10 + Integer.parseInt(idNumber.substring(3, 4)) * 5
				+ Integer.parseInt(idNumber.substring(4, 5)) * 8 + Integer.parseInt(idNumber.substring(5, 6)) * 4
				+ Integer.parseInt(idNumber.substring(6, 7)) * 2 + Integer.parseInt(idNumber.substring(7, 8)) * 1
				+ Integer.parseInt(idNumber.substring(8, 9)) * 6 + Integer.parseInt(idNumber.substring(9, 10)) * 3
				+ Integer.parseInt(idNumber.substring(10, 11)) * 7 + Integer.parseInt(idNumber.substring(11, 12)) * 9
				+ Integer.parseInt(idNumber.substring(12, 13)) * 10 + Integer.parseInt(idNumber.substring(13, 14)) * 5
				+ Integer.parseInt(idNumber.substring(14, 15)) * 8 + Integer.parseInt(idNumber.substring(15, 16)) * 4
				+ Integer.parseInt(idNumber.substring(16, 17)) * 2;
		// 计算校验
		bit = "";
		if (sum % 11 == 0) {
			bit = "1";
		} else if (sum % 11 == 1) {
			bit = "0";
		} else if (sum % 11 == 2) {
			bit = "X";
		} else if (sum % 11 == 3) {
			bit = "9";
		} else if (sum % 11 == 4) {
			bit = "8";
		} else if (sum % 11 == 5) {
			bit = "7";
		} else if (sum % 11 == 6) {
			bit = "6";
		} else if (sum % 11 == 7) {
			bit = "5";
		} else if (sum % 11 == 8) {
			bit = "4";
		} else if (sum % 11 == 9) {
			bit = "3";
		} else if (sum % 11 == 10) {
			bit = "2";
		}
		return idNumber.substring(0, 17) + bit;
	}

}
