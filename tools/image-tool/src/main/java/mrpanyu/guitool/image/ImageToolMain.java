package mrpanyu.guitool.image;

import java.util.ArrayList;
import java.util.List;

import mrpanyu.guitool.base.GuiToolRunner;

public class ImageToolMain {

	public static void main(String[] args) {
		List<Class<?>> tools = new ArrayList<>();

		tools.add(QRReader.class);
		tools.add(QRGenerator.class);
		tools.add(TesseractOCR.class);
		tools.add(IcoConverter.class);

		GuiToolRunner.run(tools, args);
	}

}
