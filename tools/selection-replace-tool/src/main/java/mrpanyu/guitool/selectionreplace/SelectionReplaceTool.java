package mrpanyu.guitool.selectionreplace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;

import com.github.kwhat.jnativehook.keyboard.NativeKeyEvent;

import mrpanyu.guitool.base.annotation.Action;
import mrpanyu.guitool.base.annotation.Parameter;
import mrpanyu.guitool.base.annotation.ToolModel;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;
import mrpanyu.guitool.ext.hotkey.Hotkey;
import mrpanyu.guitool.ext.hotkey.HotkeyTrigger;

@ToolModel(displayName = "选择替换工具", description = "注册热键后，使用Ctrl-Shift-1触发脚本-1，将当前选中文本执行脚本后用返回值进行替换。", enableProfiles = true)
public class SelectionReplaceTool {

	@Parameter(order = 0, displayName = "脚本引擎", type = ParameterType.SELECT, options = { "Groovy" })
	private String engine;

	@Parameter(order = 1, displayName = "热键类型", type = ParameterType.SELECT, options = { "Ctrl-Shift-Num",
			"Ctrl-Alt-Num", "Alt-Shift-Num" })
	private String hotkeyType = "Ctrl-Shift-Num";

	@Parameter(order = 2, displayName = "脚本-1", type = ParameterType.MULTILINE_TEXT)
	private String script1 = "return text.toUpperCase();";

	@Parameter(order = 3, displayName = "脚本-2", type = ParameterType.MULTILINE_TEXT)
	private String script2 = "return text.toLowerCase();";

	@Parameter(order = 4, displayName = "脚本-3", type = ParameterType.MULTILINE_TEXT)
	private String script3 = "return text;";

	@Parameter(order = 5, displayName = "脚本-4", type = ParameterType.MULTILINE_TEXT)
	private String script4 = "return text;";

	@Parameter(order = 6, displayName = "脚本-5", type = ParameterType.MULTILINE_TEXT)
	private String script5 = "return text;";

	private List<Hotkey> hotkeys = new ArrayList<>();

	@Action(displayName = "注册热键", order = 1)
	public void register(Tool tool) {
		if (!hotkeys.isEmpty()) {
			unregister(tool);
		}
		List<String> scripts = Arrays.asList(script1, script2, script3, script4, script5);
		ScriptEngine se = new ScriptEngineManager().getEngineByName(engine);
		se.getContext().setWriter(new ToolWriter(tool, false));
		se.getContext().setErrorWriter(new ToolWriter(tool, true));
		for (int i = 0; i < scripts.size(); i++) {
			String script = scripts.get(i);
			int keyCode = NativeKeyEvent.VC_1 + i;
			HotkeyTrigger trigger = null;
			if ("Ctrl-Shift-Num".equals(hotkeyType)) {
				trigger = new HotkeyTrigger(keyCode, true, true, false);
			} else if ("Ctrl-Alt-Num".equals(hotkeyType)) {
				trigger = new HotkeyTrigger(keyCode, true, false, true);
			} else {
				trigger = new HotkeyTrigger(keyCode, false, true, true);
			}
			SelectionReplacer replacer = new SelectionReplacer((text) -> {
				try {
					Bindings bindings = new SimpleBindings();
					bindings.put("text", text);
					String result = (String) se.eval(script, bindings);
					return result;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
			Hotkey hotkey = new Hotkey(trigger, replacer, true);
			hotkey.register();
			hotkeys.add(hotkey);
			tool.infoMessage("已注册 脚本-" + (i + 1) + " 快捷键: " + hotkeyType.replace("Num", (i + 1) + ""));
		}
	}

	@Action(displayName = "取消热键", order = 2)
	public void unregister(Tool tool) {
		for (Hotkey hotkey : hotkeys) {
			hotkey.unregister();
		}
		hotkeys.clear();
		tool.infoMessage("已取消所有热键");
	}

}
