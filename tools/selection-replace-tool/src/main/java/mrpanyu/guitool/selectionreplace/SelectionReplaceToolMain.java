package mrpanyu.guitool.selectionreplace;

import mrpanyu.guitool.base.GuiToolRunner;

public class SelectionReplaceToolMain {

	public static void main(String[] args) {
		GuiToolRunner.run(SelectionReplaceTool.class, args);
	}

}
