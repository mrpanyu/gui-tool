package mrpanyu.guitool.jasypt;

import mrpanyu.guitool.base.GuiToolRunner;

public class JasyptToolMain {

	public static void main(String[] args) {
		GuiToolRunner.run(JasyptEncryptor.class, args);
	}

}
