# 数据库代码生成工具

根据数据库模型生成各种代码的一套工具，再套上gui-tool图形界面。

目前提供的功能比较单一，主要是为了根据实际场景可以快速定制化用。

## 1. 整体设计

- model: 数据库元模型，如Table, Column之类的，所有模型都有一个ext属性，用于在处理过程中增加额外的扩展字段，例如根据表名来生成类名放入扩展字段供后续模板用。
- reader: 地区数据库元模型用的组件，可以连接JDBC反向生成，也可以做解析某些文档之类生成模型的，目前只提供MySql式和Oracle式的JDBC反向生成的，也适用于OB库。
- processor: 对数据元模型的一个单步操作，比如设置一些属性，或者用模板引擎生成一种类型的代码等。
- chain: 组装若干processor形成一套完整生成流程，例如生成JPA类代码等。chain本身也实现DbModelProcessor接口。
- DbCodeGeneratorTool: 注册所有reader/chain，形成图形界面的类。

## 2. 功能扩展

### 2.1 增加reader

1. 编写DbModelReader实现类。
2. 如果需要新增JDBC驱动，改pom.xml文件。
3. 修改DbCodeGeneratorTool.init方法，使用registerReader将新写的实现类注册进去。
4. 注：reader的属性与DbCodeGeneratorTool标注@Parameter的属性是同名对应关系，会通过setProperty方式注入进去。如有需要可以新增DbCodeGeneratorTool的参数属性。

### 2.2 增加chain

1. 编写AbstractProcessorChain实现类。
2. 如果有些单元处理需要增加processor，可以增加在processor包中，如果是没通用性的也可以直接行内箭头函数。
3. 修改DbCodeGeneratorTool.init方法，使用registerChain将新写的实现类注册进去。
4. 注：chain的属性与DbCodeGeneratorTool标注@Parameter的属性是同名对应关系，会通过setProperty方式注入进去。如有需要可以新增DbCodeGeneratorTool的参数属性。

### 2.3 模板引擎

目前程序中使用的是mvel模板引擎（及mvel脚本语言作为一些表达式判断条件）。

参考：<http://mvel.documentnode.com/>

### 2.4 其他

代码生成本身其实对gui-tool框架依赖不大，如果需要的话可以把model/reader/processor/chain的代码单独拿出来拷贝到其他工程中用，例如把功能内置到某个web工程里等等。
