package mrpanyu.guitool.dbcodegen.chain;

import java.util.Properties;

import mrpanyu.guitool.dbcodegen.processor.AbstractTemplateGenerator.TemplateFor;
import mrpanyu.guitool.dbcodegen.processor.MvelTemplateGenerator;

public class DocHtmlProcessorChain extends AbstractProcessorChain {

	private String baseDir;

	public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	@Override
	protected void setup() {
		MvelTemplateGenerator generator = new MvelTemplateGenerator();
		generator.setTemplateFor(TemplateFor.DATABASE);
		generator.setTemplateFile("classpath:/template/doc-html/doc.html.mvt");
		generator.setOutputFile("@{params.baseDir}/doc.html");
		generator.setOutputEncoding("UTF-8");
		Properties params = new Properties();
		params.setProperty("baseDir", baseDir);
		generator.setParams(params);
		addProcessor(generator);
	}

}
