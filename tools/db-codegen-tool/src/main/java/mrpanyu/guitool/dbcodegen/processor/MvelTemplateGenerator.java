package mrpanyu.guitool.dbcodegen.processor;

import java.io.Writer;
import java.util.Map;

import org.mvel2.templates.TemplateRuntime;

/**
 * 使用 MVEL({@link https://github.com/mvel/mvel}) 模板生成文件
 */
public class MvelTemplateGenerator extends AbstractTemplateGenerator {

	@Override
	protected void applyTemplate(Map<String, Object> context, String templateContent, Writer output) throws Exception {
		String result = TemplateRuntime.eval(templateContent, context).toString();
		output.write(result);
	}

}
