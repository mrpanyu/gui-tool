package mrpanyu.guitool.dbcodegen.model;

/**
 * 对应数据库列的模型
 */
@SuppressWarnings("serial")
public class Column extends Extendible {

	private String columnName;
	private String dbType;
	private int jdbcType;
	private String jdbcTypeName;
	private int length;
	private int precision;
	private int scale;
	private boolean nullable;
	private boolean primaryKey;
	private boolean foreignKey;
	private boolean unique;
	private boolean autoIncrement;
	private boolean indexed;
	private String defaultValue;
	private String comment;

	/** 列名 */
	public String getColumnName() {
		return columnName;
	}

	/** 列名 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/** 数据库列类型，如VARCHAR */
	public String getDbType() {
		return dbType;
	}

	/** 数据库列类型，如VARCHAR */
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	/** <code>java.sql.Types</code>列类型 */
	public int getJdbcType() {
		return jdbcType;
	}

	/** <code>java.sql.Types</code>列类型 */
	public void setJdbcType(int jdbcType) {
		this.jdbcType = jdbcType;
	}

	/** <code>java.sql.Types</code>列类型的枚举名称，如INTEGER */
	public String getJdbcTypeName() {
		return jdbcTypeName;
	}

	/** <code>java.sql.Types</code>列类型的枚举名称，如INTEGER */
	public void setJdbcTypeName(String jdbcTypeName) {
		this.jdbcTypeName = jdbcTypeName;
	}

	/** 文本列长度 */
	public int getLength() {
		return length;
	}

	/** 文本列长度 */
	public void setLength(int length) {
		this.length = length;
	}

	/** 数字列精度，如NUMBER(16,2)列的16 */
	public int getPrecision() {
		return precision;
	}

	/** 数字列精度，如NUMBER(16,2)列的16 */
	public void setPrecision(int precision) {
		this.precision = precision;
	}

	/** 数字列小数位数，如NUMBER(16,2)列的2 */
	public int getScale() {
		return scale;
	}

	/** 数字列小数位数，如NUMBER(16,2)列的2 */
	public void setScale(int scale) {
		this.scale = scale;
	}

	/** 是否可空 */
	public boolean isNullable() {
		return nullable;
	}

	/** 是否可空 */
	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	/** 是否主键列 */
	public boolean isPrimaryKey() {
		return primaryKey;
	}

	/** 是否主键列 */
	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	/** 是否外键列 */
	public boolean isForeignKey() {
		return foreignKey;
	}

	/** 是否外键列 */
	public void setForeignKey(boolean foreignKey) {
		this.foreignKey = foreignKey;
	}

	/** 是否唯一键列 */
	public boolean isUnique() {
		return unique;
	}

	/** 是否唯一键列 */
	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	/** 是否自增主键 */
	public boolean isAutoIncrement() {
		return autoIncrement;
	}

	/** 是否自增主键 */
	public void setAutoIncrement(boolean autoIncrement) {
		this.autoIncrement = autoIncrement;
	}

	/** 是否有索引 */
	public boolean isIndexed() {
		return indexed;
	}

	/** 是否有索引 */
	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	/** 默认值 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/** 默认值 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/** 列注释 */
	public String getComment() {
		return comment;
	}

	/** 列注释 */
	public void setComment(String comment) {
		this.comment = comment;
	}

}
