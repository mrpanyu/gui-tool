package mrpanyu.guitool.dbcodegen.chain;

import java.util.Properties;

import mrpanyu.guitool.dbcodegen.processor.AbstractTemplateGenerator.TemplateFor;
import mrpanyu.guitool.dbcodegen.processor.DefaultClassNameSetter;
import mrpanyu.guitool.dbcodegen.processor.DefaultPropertyNameSetter;
import mrpanyu.guitool.dbcodegen.processor.DefaultPropertyTypeSetter;
import mrpanyu.guitool.dbcodegen.processor.ForeignKeyReferenceSetter;
import mrpanyu.guitool.dbcodegen.processor.MvelTemplateGenerator;

public class MyBatisPlusProcessorChain extends AbstractProcessorChain {

	private String baseDir;
	private String basePackage;
	private String encoding;

	public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@Override
	protected void setup() {
		// 比较标准的几个java代码生成预处理
		addProcessor(new DefaultClassNameSetter());
		addProcessor(new DefaultPropertyNameSetter());
		addProcessor(new DefaultPropertyTypeSetter());
		addProcessor(new ForeignKeyReferenceSetter());

		// 模板中需要的属性
		Properties params = new Properties();
		params.setProperty("baseDir", baseDir);
		params.setProperty("basePackage", basePackage);
		params.setProperty("basePackagePath", basePackage.replace(".", "/"));

		// 模板引擎
		MvelTemplateGenerator generatorPo = new MvelTemplateGenerator();
		generatorPo.setTemplateFor(TemplateFor.TABLE);
		generatorPo.setTemplateFile("classpath:/template/mybatis-plus/po/Po.java.mvt");
		generatorPo.setOutputFile("${params.baseDir}/src/main/java/${params.basePackagePath}/po/@{table.ext.className}.java");
		generatorPo.setOutputEncoding(encoding);
		generatorPo.setParams(params);
		addProcessor(generatorPo);
		
		MvelTemplateGenerator generatorDao = new MvelTemplateGenerator();
		generatorDao.setTemplateFor(TemplateFor.TABLE);
		generatorDao.setTemplateFile("classpath:/template/mybatis-plus/dao/Dao.java.mvt");
		generatorDao.setOutputFile("${params.baseDir}/src/main/java/${params.basePackagePath}/dao/@{table.ext.className}Dao.java");
		generatorDao.setOutputEncoding(encoding);
		generatorDao.setParams(params);
		addProcessor(generatorDao);
		
		MvelTemplateGenerator generatorMapper = new MvelTemplateGenerator();
		generatorMapper.setTemplateFor(TemplateFor.TABLE);
		generatorMapper.setTemplateFile("classpath:/template/mybatis-plus/mapper/Dao.xml.mvt");
		generatorMapper.setOutputFile("${params.baseDir}/src/main/resources/mapper/@{table.ext.className}Dao.xml");
		generatorMapper.setOutputEncoding("UTF-8");
		generatorMapper.setParams(params);
		addProcessor(generatorMapper);
	}

}
