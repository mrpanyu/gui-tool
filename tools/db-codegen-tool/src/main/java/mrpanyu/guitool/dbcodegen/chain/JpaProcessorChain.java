package mrpanyu.guitool.dbcodegen.chain;

import java.util.Properties;

import mrpanyu.guitool.dbcodegen.processor.AbstractTemplateGenerator.TemplateFor;
import mrpanyu.guitool.dbcodegen.processor.DefaultClassNameSetter;
import mrpanyu.guitool.dbcodegen.processor.DefaultPropertyNameSetter;
import mrpanyu.guitool.dbcodegen.processor.DefaultPropertyTypeSetter;
import mrpanyu.guitool.dbcodegen.processor.ForeignKeyReferenceSetter;
import mrpanyu.guitool.dbcodegen.processor.MvelTemplateGenerator;

/**
 * 生成JPA对象
 */
public class JpaProcessorChain extends AbstractProcessorChain {

	private String baseDir;
	private String basePackage;
	private String encoding;

	public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@Override
	protected void setup() {
		// 比较标准的几个java代码生成预处理
		addProcessor(new DefaultClassNameSetter());
		addProcessor(new DefaultPropertyNameSetter());
		addProcessor(new DefaultPropertyTypeSetter());
		addProcessor(new ForeignKeyReferenceSetter());

		// 模板中需要的属性
		Properties params = new Properties();
		params.setProperty("baseDir", baseDir);
		params.setProperty("basePackage", basePackage);
		params.setProperty("basePackagePath", basePackage.replace(".", "/"));

		// 模板引擎
		MvelTemplateGenerator generator1 = new MvelTemplateGenerator();
		generator1.setTemplateFor(TemplateFor.TABLE);
		generator1.setTemplateFile("classpath:/template/jpa/ClassName.java.mvt");
		generator1.setOutputFile("@{params.baseDir}/@{params.basePackagePath}/@{table.ext.className}.java");
		generator1.setOutputEncoding(encoding);
		generator1.setParams(params);
		addProcessor(generator1);
		
		MvelTemplateGenerator generator2 = new MvelTemplateGenerator();
		generator2.setTemplateFor(TemplateFor.TABLE);
		generator2.setTemplateFile("classpath:/template/jpa/ClassNameId.java.mvt");
		generator2.setOutputFile("@{params.baseDir}/@{params.basePackagePath}/@{table.ext.className}Id.java");
		generator2.setOutputWhen("table.primaryKey != null && table.primaryKey.columnNames.size() > 1");
		generator2.setOutputEncoding(encoding);
		generator2.setParams(params);
		addProcessor(generator2);
	}

}
