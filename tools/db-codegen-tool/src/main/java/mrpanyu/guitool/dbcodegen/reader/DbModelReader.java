package mrpanyu.guitool.dbcodegen.reader;

import mrpanyu.guitool.dbcodegen.model.Database;

/**
 * 读取数据库元模型的组件接口，来源可以是JDBC连接，解析DDL文件或者某种数据文档文件等等
 */
public interface DbModelReader {
	
	Database read() throws Exception;
	
}
