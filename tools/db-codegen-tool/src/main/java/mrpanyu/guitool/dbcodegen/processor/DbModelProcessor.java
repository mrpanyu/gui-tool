package mrpanyu.guitool.dbcodegen.processor;

import mrpanyu.guitool.dbcodegen.model.Database;

/**
 * 对数据库元模型进行加工（如增加某些属性），或者用数据库元模型生成文件的组件，统称为处理组件
 */
@FunctionalInterface
public interface DbModelProcessor {

	/**
	 * 对数据库元模型进行处理
	 * 
	 * @param model 数据库元模型
	 * @return 如果返回新对象的话后续的处理组件会使用新对象，返回null则表示继续使用原来的数据库元模型对象
	 */
	Database process(Database model) throws Exception;

}
