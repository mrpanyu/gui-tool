package mrpanyu.guitool.dbcodegen.reader;

import java.sql.Connection;
import java.sql.DriverManager;

import mrpanyu.guitool.dbcodegen.util.DbCodeGeneratorUtils;

/**
 * 根据JDBC接口反向生成数据库元模型的组件
 */
public abstract class AbstractJDBCModelReader implements DbModelReader {

	protected String driverClassName;
	protected String url;
	protected String user;
	protected String password;
	protected String schema;
	protected String tableNames;

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	/**
	 * 要读取的表名，多个之间英文逗号分隔，这个是作为SQL LIKE查询的所以可以用%进行通配，例如 "T_USER_%,T_SYS_%"
	 */
	public String getTableNames() {
		return tableNames;
	}

	public void setTableNames(String tableNames) {
		this.tableNames = tableNames;
	}

	public String[] getTableNamesAsArray() {
		String[] array = null;
		if (DbCodeGeneratorUtils.isBlank(tableNames)) {
			array = new String[] { "%" };
		} else {
			array = tableNames.split(",");
			for (int i = 0; i < array.length; i++) {
				array[i] = DbCodeGeneratorUtils.trimToEmpty(array[i]);
			}
		}
		return array;
	}

	protected Connection getConnection() throws Exception {
		if (DbCodeGeneratorUtils.isNotBlank(driverClassName)) {
			Class.forName(driverClassName);
		}
		Connection conn = DriverManager.getConnection(url, user, password);
		return conn;
	}

}
