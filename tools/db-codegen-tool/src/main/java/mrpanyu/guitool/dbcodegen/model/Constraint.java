package mrpanyu.guitool.dbcodegen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 对应数据库约束的模型（主键/外键/唯一键）
 */
@SuppressWarnings("serial")
public class Constraint extends Extendible {

	private String name;
	private String indexName;
	private List<String> columnNames = new ArrayList<String>();

	/** 约束名称 */
	public String getName() {
		return name;
	}

	/** 约束名称 */
	public void setName(String name) {
		this.name = name;
	}

	/** 对应的索引名称 */
	public String getIndexName() {
		return indexName;
	}

	/** 对应的索引名称 */
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	/** 包含的列 */
	public List<String> getColumnNames() {
		return columnNames;
	}

	/** 包含的列 */
	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

}
