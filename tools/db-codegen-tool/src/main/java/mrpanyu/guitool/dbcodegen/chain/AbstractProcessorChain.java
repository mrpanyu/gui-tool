package mrpanyu.guitool.dbcodegen.chain;

import java.util.ArrayList;
import java.util.List;

import mrpanyu.guitool.dbcodegen.model.Database;
import mrpanyu.guitool.dbcodegen.processor.DbModelProcessor;

/**
 * 代码生成执行流程抽象父类
 */
public abstract class AbstractProcessorChain implements DbModelProcessor {

	private List<DbModelProcessor> processors = new ArrayList<>();

	/**
	 * 配置方法，会在每次执行前调用
	 * <p>
	 * 实现这个方法时，应该通过addProcessor方法添加需要执行的步骤，编排成流程
	 */
	protected abstract void setup();

	protected void addProcessor(DbModelProcessor processor) {
		processors.add(processor);
	}

	@Override
	public Database process(Database model) throws Exception {
		processors.clear();
		setup();

		for (DbModelProcessor processor : processors) {
			model = processor.process(model);
		}

		return model;
	}

}
