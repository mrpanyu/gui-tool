package mrpanyu.guitool.dbcodegen.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 所有模型对象的父类，包含扩展字段
 */
@SuppressWarnings("serial")
public class Extendible implements Serializable {

	private Map<String, Object> ext = new LinkedHashMap<String, Object>();

	public Map<String, Object> getExt() {
		return this.ext;
	}

	public Object getExtProperty(String name) {
		return ext.get(name);
	}

	public void setExtProperty(String name, Object value) {
		ext.put(name, value);
	}

}
