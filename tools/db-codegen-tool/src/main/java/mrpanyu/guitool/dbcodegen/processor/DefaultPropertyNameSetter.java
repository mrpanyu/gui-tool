package mrpanyu.guitool.dbcodegen.processor;

import mrpanyu.guitool.dbcodegen.model.Column;
import mrpanyu.guitool.dbcodegen.model.Database;
import mrpanyu.guitool.dbcodegen.model.Table;
import mrpanyu.guitool.dbcodegen.util.DbCodeGeneratorUtils;

/**
 * 给<code>Column</code>对象设置两个额外属性：
 * <ul>
 * <li>propertyName</li>
 * <li>propertyNamePascal</li>
 * </ul>
 */
public class DefaultPropertyNameSetter implements DbModelProcessor {

	@Override
	public Database process(Database model) throws Exception {
		for (Table table : model.getTables()) {
			for (Column column : table.getColumns()) {
				String columnName = column.getColumnName();
				columnName = columnName.toLowerCase();
				String[] arrColumnName = columnName.split("_");
				StringBuilder sb = new StringBuilder();
				for (String part : arrColumnName) {
					if (DbCodeGeneratorUtils.isNotBlank(part)) {
						sb.append(part.substring(0, 1).toUpperCase()).append(part.substring(1));
					}
				}
				String propertyNamePascal = sb.toString();
				String propertyName = sb.substring(0, 1).toLowerCase() + sb.substring(1);
				column.setExtProperty("propertyName", propertyName);
				column.setExtProperty("propertyNamePascal", propertyNamePascal);
			}
		}
		return model;
	}

}
