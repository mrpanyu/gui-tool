package mrpanyu.guitool.dbcodegen;

import mrpanyu.guitool.base.GuiToolRunner;

public class DbCodeGeneratorToolMain {
	
	public static void main(String[] args) {
		GuiToolRunner.run(DbCodeGeneratorTool.class, args);
	}
	
}
