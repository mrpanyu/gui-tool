package mrpanyu.guitool.dbcodegen.processor;

import mrpanyu.guitool.dbcodegen.model.Database;
import mrpanyu.guitool.dbcodegen.model.Table;
import mrpanyu.guitool.dbcodegen.util.DbCodeGeneratorUtils;

/**
 * 在<code>Table</code>对象中设置两个额外属性：
 * <ul>
 * <li>className</li>
 * <li>classNameCamel</li>
 * </ul>
 * <p>
 * 可以设置去掉表名前缀，比如"T_"之类的。
 *
 */
public class DefaultClassNameSetter implements DbModelProcessor {

	private String tableNamePrefix = "";

	public String getTableNamePrefix() {
		return tableNamePrefix;
	}

	public void setTableNamePrefix(String tableNamePrefix) {
		this.tableNamePrefix = tableNamePrefix;
	}

	@Override
	public Database process(Database model) throws Exception {
		for (Table table : model.getTables()) {
			String tableName = table.getTableName();
			tableName = tableName.toLowerCase();
			if (DbCodeGeneratorUtils.isNotBlank(tableNamePrefix)
					&& tableName.startsWith(tableNamePrefix.toLowerCase())) {
				tableName = tableName.substring(tableNamePrefix.length());
			}
			String[] arrTableName = tableName.split("_");
			StringBuilder sb = new StringBuilder();
			for (String part : arrTableName) {
				if (DbCodeGeneratorUtils.isNotBlank(part)) {
					sb.append(part.substring(0, 1).toUpperCase()).append(part.substring(1));
				}
			}
			table.setExtProperty("className", sb.toString());
			table.setExtProperty("classNameCamel", sb.substring(0, 1).toLowerCase() + sb.substring(1));
		}
		return model;
	}

}
