package mrpanyu.guitool.dbcodegen.processor;

import mrpanyu.guitool.dbcodegen.model.Column;
import mrpanyu.guitool.dbcodegen.model.Database;
import mrpanyu.guitool.dbcodegen.model.Table;

/**
 * 给每个表每个列设置"propertyType"额外属性的处理组件
 */
public abstract class AbstractPropertyTypeSetter implements DbModelProcessor {

	public Database process(Database model) throws Exception {
		for (Table table : model.getTables()) {
			for (Column column : table.getColumns()) {
				String dbType = column.getDbType();
				int jdbcType = column.getJdbcType();
				String propertyType = getPropertyType(dbType, jdbcType);
				column.setExtProperty("propertyType", propertyType);
			}
		}
		return model;
	}

	protected abstract String getPropertyType(String dbType, int jdbcType);

}
