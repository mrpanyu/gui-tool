package mrpanyu.guitool.dbcodegen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 对应数据表的模型
 */
@SuppressWarnings("serial")
public class Table extends Extendible {

	private String tableName;
	private String comment;
	private List<Column> columns = new ArrayList<Column>();
	private PrimaryKey primaryKey;
	private List<ForeignKey> foreignKeys = new ArrayList<ForeignKey>();
	private List<Unique> uniques = new ArrayList<Unique>();

	/** 表名 */
	public String getTableName() {
		return tableName;
	}

	/** 表名 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/** 表注释 */
	public String getComment() {
		return comment;
	}

	/** 表注释 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/** 包含的列 */
	public List<Column> getColumns() {
		return columns;
	}

	/** 包含的列 */
	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	/** 主键约束 */
	public PrimaryKey getPrimaryKey() {
		return primaryKey;
	}

	/** 主键约束 */
	public void setPrimaryKey(PrimaryKey primaryKey) {
		this.primaryKey = primaryKey;
	}

	/** 所有外键约束 */
	public List<ForeignKey> getForeignKeys() {
		return foreignKeys;
	}

	/** 所有外键约束 */
	public void setForeignKeys(List<ForeignKey> foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	/** 所有唯一键约束 */
	public List<Unique> getUniques() {
		return uniques;
	}

	/** 所有唯一键约束 */
	public void setUniques(List<Unique> uniques) {
		this.uniques = uniques;
	}

}
