package mrpanyu.guitool.dbcodegen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据库的模型对象，目前只包含了表
 */
@SuppressWarnings("serial")
public class Database extends Extendible {

	private List<Table> tables = new ArrayList<Table>();

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

}
