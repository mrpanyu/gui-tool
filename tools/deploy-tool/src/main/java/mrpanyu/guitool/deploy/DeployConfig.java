package mrpanyu.guitool.deploy;

import java.util.LinkedHashMap;

public class DeployConfig {

	private LinkedHashMap<String, ServerInfo> servers;
	private LinkedHashMap<String, DeployTask> tasks;

	public LinkedHashMap<String, ServerInfo> getServers() {
		return servers;
	}

	public void setServers(LinkedHashMap<String, ServerInfo> servers) {
		this.servers = servers;
	}

	public LinkedHashMap<String, DeployTask> getTasks() {
		return tasks;
	}

	public void setTasks(LinkedHashMap<String, DeployTask> tasks) {
		this.tasks = tasks;
	}

}
