package mrpanyu.guitool.deploy;

import mrpanyu.guitool.base.GuiToolRunner;

public class DeployToolMain {

	public static void main(String[] args) {
		GuiToolRunner.run(DeployTool.class, args);
	}

}
