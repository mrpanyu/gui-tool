package mrpanyu.guitool.deploy;

import java.util.List;

public class DeployTask {

	private String server;
	private UploadInfo upload;
	private List<String> commands;
	private List<String> dependTasks;

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public UploadInfo getUpload() {
		return upload;
	}

	public void setUpload(UploadInfo upload) {
		this.upload = upload;
	}

	public List<String> getCommands() {
		return commands;
	}

	public void setCommands(List<String> commands) {
		this.commands = commands;
	}

	public List<String> getDependTasks() {
		return dependTasks;
	}

	public void setDependTasks(List<String> dependTasks) {
		this.dependTasks = dependTasks;
	}

}
