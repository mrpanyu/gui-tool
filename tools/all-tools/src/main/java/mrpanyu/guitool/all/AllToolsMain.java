package mrpanyu.guitool.all;

import java.util.ArrayList;
import java.util.List;

import mrpanyu.guitool.base.GuiToolRunner;
import mrpanyu.guitool.bizno.IdNoGenerator;
import mrpanyu.guitool.bizno.RandomNoGenerator;
import mrpanyu.guitool.crypt.DigestTool;
import mrpanyu.guitool.crypt.Sm2Tool;
import mrpanyu.guitool.crypt.Sm4Tool;
import mrpanyu.guitool.image.IcoConverter;
import mrpanyu.guitool.image.QRGenerator;
import mrpanyu.guitool.image.QRReader;
import mrpanyu.guitool.image.TesseractOCR;
import mrpanyu.guitool.jasypt.JasyptEncryptor;
import mrpanyu.guitool.jwt.JwtTool;
import mrpanyu.guitool.script.ScriptTool;
import mrpanyu.guitool.textfile.Base64Tool;
import mrpanyu.guitool.textfile.EscapeTool;
import mrpanyu.guitool.textfile.ExtractByFileList;
import mrpanyu.guitool.textfile.SplitJavaAndResourceFileTool;

public class AllToolsMain {

	public static void main(String[] args) {
		List<Class<?>> tools = new ArrayList<>();

		// script-tool
		tools.add(ScriptTool.class);
		// bizno-tool
		tools.add(IdNoGenerator.class);
		tools.add(RandomNoGenerator.class);
		// textfile-tool
		tools.add(Base64Tool.class);
		tools.add(EscapeTool.class);
		tools.add(ExtractByFileList.class);
		tools.add(SplitJavaAndResourceFileTool.class);
		// crypt-tool
		tools.add(DigestTool.class);
		tools.add(Sm2Tool.class);
		tools.add(Sm4Tool.class);
		// image-tool
		tools.add(QRReader.class);
		tools.add(QRGenerator.class);
		tools.add(TesseractOCR.class);
		tools.add(IcoConverter.class);
		// jasypt-tool
		tools.add(JasyptEncryptor.class);
		// jwt-tool
		tools.add(JwtTool.class);

		GuiToolRunner.run(tools, args);
	}

}
