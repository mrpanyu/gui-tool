package mrpanyu.guitool.yunduan;

import java.io.File;

import mrpanyu.guitool.base.annotation.Action;
import mrpanyu.guitool.base.annotation.Parameter;
import mrpanyu.guitool.base.annotation.ToolModel;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;

@ToolModel(displayName = "云端导出-VM端")
public class YunduanExportVM {

	@Parameter(displayName = "待导出文件", type = ParameterType.FILE, order = 1)
	private File file;

	@Parameter(displayName = "图像大小", order = 2)
	private int imgSize = 1024;

	@Parameter(displayName = "分片大小", order = 3)
	private int pageSize = 1024;

	@Action(displayName = "导出")
	public void export(Tool tool) {
		if (file == null) {
			tool.errorMessage("导出文件不能为空");
			return;
		}
		YunduanExportFrame f = new YunduanExportFrame(file, imgSize, pageSize);
		f.setVisible(true);
	}

}
