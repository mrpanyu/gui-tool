package mrpanyu.guitool.yunduan;

import java.util.ArrayList;
import java.util.List;

import mrpanyu.guitool.base.GuiToolRunner;

public class YunduanToolMain {

	public static void main(String[] args) {
		List<Class<?>> tools = new ArrayList<>();

		tools.add(QRGenerator.class);
		tools.add(QRReader.class);
		tools.add(YunduanImportHost.class);
		tools.add(YunduanImportVM.class);
		tools.add(YunduanExportHost.class);
		tools.add(YunduanExportVM.class);

		GuiToolRunner.run(tools, args);
	}

}
