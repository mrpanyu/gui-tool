/** npm install --save sm-crypto */

/* SM2程序示例 */
const sm2 = require('sm-crypto').sm2;

/*
 * 示例的这组密钥对，在java端的pkcs8/x509编码格式：
 * 私钥：308193020100301306072a8648ce3d020106082a811ccf5501822d047930770201010420be57226ed94b51cf776aceb3792dad673c664a4130c145c3dfb2e1cde9fa4dd3a00a06082a811ccf5501822da14403420004b400094231aa4698346540d60b1e3e5f2947da7595cb5caae633b5324d206a4ef76ce817b64665be2acd1e001f5864ca92569f8bd360668b57eeedd079c58fa1
 * 公钥：3059301306072a8648ce3d020106082a811ccf5501822d03420004b400094231aa4698346540d60b1e3e5f2947da7595cb5caae633b5324d206a4ef76ce817b64665be2acd1e001f5864ca92569f8bd360668b57eeedd079c58fa1
 */

// 私钥使用D值Hex
let privateKey = 'be57226ed94b51cf776aceb3792dad673c664a4130c145c3dfb2e1cde9fa4dd3';
// 公钥使用Q值Hex，非压缩的
let publicKey = '04b400094231aa4698346540d60b1e3e5f2947da7595cb5caae633b5324d206a4ef76ce817b64665be2acd1e001f5864ca92569f8bd360668b57eeedd079c58fa1';

// 1 - C1C3C2，0 - C1C2C3，默认为1
const cipherMode = 1;

let text = "测试文本内容"; // 原始字符串，会当作UTF-8编码加密
let encData = sm2.doEncrypt(text, publicKey, cipherMode); // 公钥加密
let text2 = sm2.doDecrypt(encData, privateKey, cipherMode); // 私钥解密
let signature = sm2.doSignature(text, privateKey, { der:true, hash:true }); // 私钥签名
let verifyResult = sm2.doVerifySignature(text, signature, publicKey, { der:true, hash:true }); // 验证签名

/*
 * 注：加密出来的encData为一个Hex格式的字符串，注意这个字符串如果要在java程序中用对应的私钥解密，需要前面加上固定前缀"04"才可以。
 * 同样java程序中加密出来的内容，要转成Hex格式字符串，然后去掉"04"前缀才能在js中解密。
 *
 * 生成的签名同样是Hex格式，由于java端生成签名是ASN1格式的，因此js这里做签名的时候要设置成 { der:true, hash:true }
 */

console.log("Text: " + text);
console.log("EncData: " + encData);
console.log("Text2: " + text2);
console.log("Signature: " + signature);
console.log("VerifyResult: " + verifyResult);

/* SM4程序示例 */
const sm4 = require('sm-crypto').sm4;

/* 对称密钥的Hex格式，SM4需要128bit的密钥 */
let key = '045bf55342bcda664fd50f80b54979df';
/* IV的Hex格式，SM4需要128bit的IV */
let iv = '227cc68f183f8f0a6055932500ff5586';

let txt = '测试文本内容'; // 待加密字符串
let enc = sm4.encrypt(txt, key, {mode: 'cbc', padding:'pkcs#5', iv: iv}); // sm4加密
let txt2 = sm4.decrypt(enc, key, {mode: 'cbc', padding:'pkcs#5', iv: iv}); // sm4解密

/*
 * SM4加解密与java端没有太大出入，mode和padding与java端保持一样，key/iv相同就可以正常加解密。
 * mode 应该是 cbc 或 ecb
 * padding 应该是 pkcs#5（同 pkcs#7）或 none
 */

console.log('Txt: ' + txt);
console.log('Enc: ' + enc);
console.log('Txt2: ' + txt2);
