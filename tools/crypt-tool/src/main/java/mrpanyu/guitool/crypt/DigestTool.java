package mrpanyu.guitool.crypt;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.zip.CRC32;

import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.digest.DigesterFactory;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.crypto.digest.SM3;
import mrpanyu.guitool.base.annotation.Action;
import mrpanyu.guitool.base.annotation.OnParameterChange;
import mrpanyu.guitool.base.annotation.Parameter;
import mrpanyu.guitool.base.annotation.ToolModel;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;

@ToolModel(displayName = "摘要计算(MD5/SHA/SM3/CRC32)", description = "计算MD5/SHA等摘要值", enableProfiles = false)
public class DigestTool {

	@Parameter(displayName = "摘要算法", type = ParameterType.SELECT, options = { "MD5", "SM3", "CRC32", "SHA-1", "SHA-256",
			"SHA-384", "SHA-512" }, order = 1)
	private String algorithm = "MD5";

	@Parameter(displayName = "内容来源", type = ParameterType.SELECT, options = { "手工输入", "选择文件" }, order = 2)
	private String source = "手工输入";

	@Parameter(displayName = "输入文本", type = ParameterType.MULTILINE_TEXT, order = 3)
	private String textValue = "";

	@Parameter(displayName = "字符集", type = ParameterType.TEXT, order = 4)
	private String textEncoding = "UTF-8";

	@Parameter(displayName = "选择文件", type = ParameterType.FILE, order = 5, visible = false)
	private File file;

	@OnParameterChange("source")
	public void onSourceChange(Tool tool) {
		if ("手工输入".equals(source)) {
			tool.getParameter("textValue").setVisible(true);
			tool.getParameter("textEncoding").setVisible(true);
			tool.getParameter("fileName").setVisible(false);
		} else {
			tool.getParameter("textValue").setVisible(false);
			tool.getParameter("textEncoding").setVisible(false);
			tool.getParameter("fileName").setVisible(true);
		}
	}

	@Action(displayName = "计算", order = 1)
	public void calc(Tool tool) {
		byte[] data = null;
		if ("手工输入".equals(source)) {
			try {
				data = textValue.getBytes(textEncoding);
			} catch (UnsupportedEncodingException e) {
				tool.errorMessage("字符集无效: " + textEncoding);
				return;
			}
		} else {
			if (file == null || !file.isFile()) {
				tool.errorMessage("文件不存在: " + file);
				return;
			}
			data = FileUtil.readBytes(file);
		}
		String digest = null;
		String digestDigital = null;
		if ("MD5".equals(algorithm)) {
			digest = MD5.create().digestHex(data);
		} else if ("SM3".equals(algorithm)) {
			digest = SM3.create().digestHex(data);
		} else if ("CRC32".equals(algorithm)) {
			CRC32 crc32 = new CRC32();
			crc32.update(data);
			digest = Long.toHexString(crc32.getValue());
			digestDigital = Long.toString(crc32.getValue());
		} else {
			digest = DigesterFactory.of(algorithm).createDigester().digestHex(data);
		}
		tool.infoMessage(algorithm + "摘要Hex(小写): " + digest.toLowerCase());
		tool.infoMessage(algorithm + "摘要Hex(大写): " + digest.toUpperCase());
		if (digestDigital != null) {
			tool.infoMessage(algorithm + "摘要十进制: " + digestDigital);
		}
	}

	@Action(displayName = "清空", order = 2)
	public void clear(Tool tool) {
		tool.clearMessages();
	}

}
