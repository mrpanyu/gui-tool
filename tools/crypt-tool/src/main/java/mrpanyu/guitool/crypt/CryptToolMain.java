package mrpanyu.guitool.crypt;

import java.util.ArrayList;
import java.util.List;

import mrpanyu.guitool.base.GuiToolRunner;

public class CryptToolMain {

	public static void main(String[] args) {
		List<Class<?>> tools = new ArrayList<>();

		tools.add(DigestTool.class);
		tools.add(Sm2Tool.class);
		tools.add(Sm4Tool.class);

		GuiToolRunner.run(tools, args);
	}

}
