package mrpanyu.guitool.base.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface Action {

	public String displayName() default "";

	public String description() default "";

	public boolean visible() default true;

	public int order() default 0;

}
