package mrpanyu.guitool.base.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import mrpanyu.guitool.base.model.ParameterType;

@Retention(RUNTIME)
@Target(FIELD)
public @interface Parameter {

	public String displayName() default "";

	public String description() default "";

	public ParameterType type() default ParameterType.TEXT;

	public String[] options() default {};

	public boolean visible() default true;

	public int order() default 0;

}
