package mrpanyu.guitool.base.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface ToolModel {

	public String displayName() default "";

	public String description() default "";

	public boolean enableProfiles() default true;

}
