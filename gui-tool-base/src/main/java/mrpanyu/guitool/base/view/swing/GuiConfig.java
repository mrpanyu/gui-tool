package mrpanyu.guitool.base.view.swing;

public class GuiConfig {

	private int windowWidth = 1280;
	private int windowHeight = 1024;
	private int multiToolMenuWidth = 250;
	private int lineHeight = 32;
	private int labelWidth = 200;
	private int fontSize = 18;
	private int messageFontSize = 16;
	private int buttonImgSize = 20;
	private int maxLogRows = 4000;

	public int getWindowWidth() {
		return windowWidth;
	}

	public GuiConfig setWindowWidth(int windowWidth) {
		this.windowWidth = windowWidth;
		return this;
	}

	public int getWindowHeight() {
		return windowHeight;
	}

	public GuiConfig setWindowHeight(int windowHeight) {
		this.windowHeight = windowHeight;
		return this;
	}

	public int getMultiToolMenuWidth() {
		return multiToolMenuWidth;
	}

	public void setMultiToolMenuWidth(int multiToolMenuWidth) {
		this.multiToolMenuWidth = multiToolMenuWidth;
	}

	public int getLineHeight() {
		return lineHeight;
	}

	public GuiConfig setLineHeight(int lineHeight) {
		this.lineHeight = lineHeight;
		return this;
	}

	public int getLabelWidth() {
		return labelWidth;
	}

	public GuiConfig setLabelWidth(int labelWidth) {
		this.labelWidth = labelWidth;
		return this;
	}

	public int getFontSize() {
		return fontSize;
	}

	public GuiConfig setFontSize(int fontSize) {
		this.fontSize = fontSize;
		return this;
	}

	public int getMessageFontSize() {
		return messageFontSize;
	}

	public GuiConfig setMessageFontSize(int messageFontSize) {
		this.messageFontSize = messageFontSize;
		return this;
	}

	public int getButtonImgSize() {
		return buttonImgSize;
	}

	public GuiConfig setButtonImgSize(int buttonImgSize) {
		this.buttonImgSize = buttonImgSize;
		return this;
	}

	public int getMaxLogRows() {
		return maxLogRows;
	}

	public void setMaxLogRows(int maxLogRows) {
		this.maxLogRows = maxLogRows;
	}

	public GuiConfig scale(double scale) {
		this.windowWidth = (int) (this.windowWidth * scale);
		this.windowHeight = (int) (this.windowHeight * scale);
		this.multiToolMenuWidth = (int) (this.multiToolMenuWidth * scale);
		this.lineHeight = (int) (this.lineHeight * scale);
		this.labelWidth = (int) (this.labelWidth * scale);
		this.fontSize = (int) (this.fontSize * scale);
		this.messageFontSize = (int) (this.messageFontSize * scale);
		this.buttonImgSize = (int) (this.buttonImgSize * scale);
		return this;
	}

}
