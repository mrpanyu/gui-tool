package mrpanyu.guitool.base.view.swing;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import mrpanyu.guitool.base.model.ToolModel;
import mrpanyu.guitool.base.util.Log;

public class GuiToolFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private static GuiToolFrame instance;

	public static GuiToolFrame getInstance() {
		return instance;
	}

	private JPanel mainPanel;

	public GuiToolFrame(ToolModel toolModel) {
		this(toolModel, null);
	}

	public GuiToolFrame(ToolModel toolModel, GuiConfig guiConfig) {
		if (guiConfig == null) {
			guiConfig = new GuiConfig();
		}
		GuiToolPanelBuilder tpb = new GuiToolPanelBuilder(toolModel, guiConfig);
		Log.setTool(tpb);
		mainPanel = tpb.getJPanel();
		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.CENTER);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle(toolModel.getDisplayName());
		this.setIconImage(new ImageIcon(this.getClass().getResource("images/tools.png")).getImage());
		this.setLocationByPlatform(false);
		this.setLocationRelativeTo(null);
		instance = this;
		tpb.onload();
	}

}
