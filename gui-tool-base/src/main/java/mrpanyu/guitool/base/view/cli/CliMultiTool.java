package mrpanyu.guitool.base.view.cli;

import java.util.List;

import mrpanyu.guitool.base.model.ToolModel;

public class CliMultiTool {

	private List<ToolModel> toolModels;

	public CliMultiTool(List<ToolModel> toolModels) {
		this.toolModels = toolModels;
	}

	public void run() {
		CliCommons.coloredPrintln("------ Choose Tool ------", CliCommons.ANSI_GREEN);
		for (int i = 0; i < toolModels.size(); i++) {
			System.out.println("  [Tool] " + (i + 1) + " - " + toolModels.get(i).getDisplayName());
		}
		int n = CliCommons.readNum("  Choose number (1-" + toolModels.size() + "): ", toolModels.size());
		ToolModel toolModel = toolModels.get(n - 1);
		CliTool cli = new CliTool(toolModel);
		cli.run();
	}

}
