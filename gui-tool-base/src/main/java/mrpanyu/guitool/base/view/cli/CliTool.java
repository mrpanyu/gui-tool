package mrpanyu.guitool.base.view.cli;

import java.util.List;
import java.util.stream.Collectors;

import mrpanyu.guitool.base.model.Action;
import mrpanyu.guitool.base.model.Parameter;
import mrpanyu.guitool.base.model.ParameterType;
import mrpanyu.guitool.base.model.Tool;
import mrpanyu.guitool.base.model.ToolModel;
import mrpanyu.guitool.base.util.Log;

public class CliTool extends Tool {

	public CliTool(ToolModel toolModel) {
		super(toolModel);
	}

	public void run() {
		Log.setTool(this);
		toolModel.onload(this);
		inputParameters();
		doAction();
	}

	@Override
	public void debugMessage(String message) {
		if (isDebugEnabled()) {
			CliCommons.coloredMessage(message, "DEBUG", CliCommons.ANSI_BLUE);
		}
	}

	@Override
	public void infoMessage(String message) {
		CliCommons.coloredMessage(message, "INFO ", CliCommons.ANSI_BLUE);
	}

	@Override
	public void warnMessage(String message) {
		CliCommons.coloredMessage(message, "WARN ", CliCommons.ANSI_YELLOW);
	}

	@Override
	public void errorMessage(String message) {
		CliCommons.coloredMessage(message, "ERROR", CliCommons.ANSI_RED);
	}

	@Override
	public void errorMessage(Throwable t) {
		CliCommons.coloredMessage("", "ERROR", CliCommons.ANSI_RED);
		t.printStackTrace();
	}

	@Override
	public void htmlMessage(String message) {
		CliCommons.coloredMessage("", "HTML ", CliCommons.ANSI_BLUE);
	}

	@Override
	public void clearMessages() {
		// Unsupported
	}

	private void inputParameters() {
		CliCommons.coloredPrintln("------ Input parameters ------", CliCommons.ANSI_GREEN);
		for (Parameter param : toolModel.getParameters()) {
			String name = param.getDisplayName();
			if (!param.isVisible()) {
				continue;
			}
			if (param.getType() == ParameterType.SELECT) {
				System.out.println(name + ":");
				for (int i = 0; i < param.getOptions().size(); i++) {
					System.out.println("  [Option] " + (i + 1) + " - " + param.getOptions().get(i));
				}
				int n = CliCommons.readNum("  Choose number (1-" + param.getOptions().size() + "): ",
						param.getOptions().size());
				param.setValue(param.getOptions().get(n - 1));
			} else {
				String input = CliCommons.readLine(name + ": ");
				param.setValue(input);
			}
			if (param.getChangeHandler() != null) {
				try {
					param.getChangeHandler().onParameterChange(param, this);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void doAction() {
		CliCommons.coloredPrintln("------ Choose action ------", CliCommons.ANSI_GREEN);
		System.out.println("Choose action to execute: ");
		List<Action> actions = toolModel.getActions().stream().filter(a -> a.isVisible()).collect(Collectors.toList());
		for (int i = 0; i < actions.size(); i++) {
			System.out.println("  [Action] " + (i + 1) + " - " + actions.get(i).getDisplayName());
		}
		int n = CliCommons.readNum("  Choose number (1-" + actions.size() + "): ", actions.size());
		Action action = actions.get(n - 1);
		try {
			action.getHandler().handle(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
