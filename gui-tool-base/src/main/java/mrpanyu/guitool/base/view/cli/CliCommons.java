package mrpanyu.guitool.base.view.cli;

import java.util.Scanner;

public class CliCommons {

	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	private static Scanner scanner = new Scanner(System.in);

	public static void coloredPrintln(String message, String ansiColor) {
		System.out.println(ansiColor + message + CliCommons.ANSI_RESET);
	}

	public static void coloredMessage(String message, String level, String ansiColor) {
		System.out.println(ansiColor + "[" + level + "] " + CliCommons.ANSI_RESET + message);
	}

	public static String readLine(String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}

	public static int readNum(String prompt, int maxValue) {
		int num = -1;
		do {
			System.out.print(prompt);
			String line = scanner.nextLine();
			try {
				num = Integer.parseInt(line);
			} catch (Exception e) {
				// ignore
			}
		} while (num <= 0 || num > maxValue);
		return num;
	}
}
