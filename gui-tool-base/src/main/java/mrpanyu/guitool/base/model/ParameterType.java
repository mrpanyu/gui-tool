package mrpanyu.guitool.base.model;

public enum ParameterType {
	TEXT, MULTILINE_TEXT, MULTILINE_TEXT_LINEWRAP, SELECT, FILE, DIRECTORY
}
