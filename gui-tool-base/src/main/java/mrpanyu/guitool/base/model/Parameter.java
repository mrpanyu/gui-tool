package mrpanyu.guitool.base.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Parameter {

	private String name;
	private String displayName;
	private String description;
	private ParameterType type;
	private String value;
	private List<String> options = new ArrayList<>();
	private boolean visible = true;
	private ParameterChangeHandler changeHandler;

	public String getName() {
		return name;
	}

	public Parameter setName(String name) {
		this.name = name;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Parameter setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Parameter setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getValue() {
		return value;
	}

	public Parameter setValue(String value) {
		this.value = value;
		return this;
	}

	public ParameterType getType() {
		return type;
	}

	public Parameter setType(ParameterType type) {
		this.type = type;
		return this;
	}

	public List<String> getOptions() {
		return options;
	}

	public Parameter setOptions(List<String> options) {
		this.options = options;
		if (options != null && !options.isEmpty()) {
			if (!options.contains(this.value)) {
				this.value = options.get(0);
			}
		}
		return this;
	}

	public Parameter setOptions(String... options) {
		return setOptions(Arrays.asList(options));
	}

	public boolean isVisible() {
		return visible;
	}

	public Parameter setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}

	public ParameterChangeHandler getChangeHandler() {
		return changeHandler;
	}

	public Parameter setChangeHandler(ParameterChangeHandler changeHandler) {
		this.changeHandler = changeHandler;
		return this;
	}

}
