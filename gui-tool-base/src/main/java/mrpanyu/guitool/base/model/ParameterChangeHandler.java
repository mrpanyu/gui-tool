package mrpanyu.guitool.base.model;

@FunctionalInterface
public interface ParameterChangeHandler {

	void onParameterChange(Parameter parameter, Tool tool) throws Exception;

}
