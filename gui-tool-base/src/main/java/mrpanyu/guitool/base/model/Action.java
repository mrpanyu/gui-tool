package mrpanyu.guitool.base.model;

public class Action {

	private String name;
	private String displayName;
	private String description;
	private boolean visible = true;
	private ActionHandler handler;

	public String getName() {
		return name;
	}

	public Action setName(String name) {
		this.name = name;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Action setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public boolean isVisible() {
		return visible;
	}

	public Action setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Action setDescription(String description) {
		this.description = description;
		return this;
	}

	public ActionHandler getHandler() {
		return handler;
	}

	public void setHandler(ActionHandler handler) {
		this.handler = handler;
	}

}
