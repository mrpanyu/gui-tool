package mrpanyu.guitool.base.model;

import java.util.List;

public abstract class Tool {

	protected ToolModel toolModel;

	public Tool(ToolModel toolModel) {
		this.toolModel = toolModel;
	}

	public String getName() {
		return toolModel.getName();
	}

	public String getDisplayName() {
		return toolModel.getDisplayName();
	}

	public String getDescription() {
		return toolModel.getDescription();
	}

	public List<Parameter> getParameters() {
		return toolModel.getParameters();
	}

	public Parameter getParameter(String name) {
		return toolModel.getParameters().stream().filter(p -> p.getName().equals(name)).findFirst().get();
	}

	public String getParameterValue(String name) {
		return getParameter(name).getValue();
	}

	public void setParameterValue(String name, String value) {
		getParameter(name).setValue(value);
	}

	public void setParameterOptions(String name, List<String> options) {
		getParameter(name).setOptions(options);
	}

	public List<Action> getActions() {
		return toolModel.getActions();
	}

	public Action getAction(String name) {
		return toolModel.getActions().stream().filter(a -> a.getName().equals(name)).findFirst().get();
	}

	public boolean isDebugEnabled() {
		String mode = System.getProperty("mode");
		return "debug".equalsIgnoreCase(mode);
	}

	public abstract void debugMessage(String message);

	public abstract void infoMessage(String message);

	public abstract void warnMessage(String message);

	public abstract void errorMessage(String message);

	public abstract void errorMessage(Throwable t);

	public abstract void htmlMessage(String message);

	public abstract void clearMessages();

}
