package mrpanyu.guitool.base.model;

@FunctionalInterface
public interface ActionHandler {

	public void handle(Tool tool) throws Exception;

}
