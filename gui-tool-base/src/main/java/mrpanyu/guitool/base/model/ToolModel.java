package mrpanyu.guitool.base.model;

import java.util.ArrayList;
import java.util.List;

public class ToolModel {

	private String name;
	private String displayName;
	private String description;
	private List<Parameter> parameters = new ArrayList<>();
	private List<Action> actions = new ArrayList<>();
	private ActionHandler onloadHandler;
	private boolean enableProfiles = false;

	public String getName() {
		return name;
	}

	public ToolModel setName(String name) {
		this.name = name;
		return this;
	}

	public String getDisplayName() {
		return displayName;
	}

	public ToolModel setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public ToolModel setDescription(String description) {
		this.description = description;
		return this;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public ToolModel setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
		return this;
	}

	public List<Action> getActions() {
		return actions;
	}

	public ToolModel setActions(List<Action> actions) {
		this.actions = actions;
		return this;
	}

	public Parameter addParameter(Parameter parameter) {
		this.parameters.add(parameter);
		return parameter;
	}

	public Action addAction(Action action) {
		this.actions.add(action);
		return action;
	}

	public void onload(Tool tool) {
		if (onloadHandler != null) {
			try {
				onloadHandler.handle(tool);
			} catch (Exception e) {
				tool.errorMessage(e);
			}
		}
	}

	public ActionHandler getOnloadHandler() {
		return onloadHandler;
	}

	public void setOnloadHandler(ActionHandler onloadHandler) {
		this.onloadHandler = onloadHandler;
	}

	public boolean isEnableProfiles() {
		return enableProfiles;
	}

	public ToolModel setEnableProfiles(boolean enableProfiles) {
		this.enableProfiles = enableProfiles;
		return this;
	}

}
