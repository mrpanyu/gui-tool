package mrpanyu.guitool.base.util;

import mrpanyu.guitool.base.model.Tool;

public final class Log {

	private static Tool tool;

	public static void setTool(Tool t) {
		tool = t;
	}

	public static boolean isDebugEnabled() {
		return tool.isDebugEnabled();
	}

	public static void debug(String message) {
		tool.debugMessage(message);
	}

	public static void info(String message) {
		tool.infoMessage(message);
	}

	public static void warn(String message) {
		tool.warnMessage(message);
	}

	public static void error(String message) {
		tool.errorMessage(message);
	}

	public static void error(Throwable t) {
		tool.errorMessage(t);
	}

	public static void error(String message, Throwable t) {
		tool.errorMessage(message);
		tool.errorMessage(t);
	}

	private Log() {
	}

}
