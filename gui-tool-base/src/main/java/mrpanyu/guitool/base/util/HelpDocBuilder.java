package mrpanyu.guitool.base.util;

import mrpanyu.guitool.base.model.Action;
import mrpanyu.guitool.base.model.Parameter;
import mrpanyu.guitool.base.model.ToolModel;

public class HelpDocBuilder {

	public static String buildHelp(ToolModel toolModel) {
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><style type='text/css'>");
		sb.append(".toolname {font-size:20px;font-weight:bold;color:#0000FF}");
		sb.append(".tooldesc {font-size:14px;color:#000000}");
		sb.append(
				".paramstitle, .actionstitle, .cmdtitle {font-size:18px;font-weight:bold;color:#00CC00;margin-top:20px;}");
		sb.append(".paramslist, .actionslist {font-size:13px;}");
		sb.append(".paramname, .actionname {font-weight:bold;color:#0000FF}");
		sb.append(".cmddesc {font-size:13px;padding-left:10px;}");
		sb.append("</style></head><body>");
		sb.append("<div class='toolname'>").append(toolModel.getDisplayName()).append("</div>");
		sb.append("<div class='tooldesc'>").append(trim(toolModel.getDescription())).append("</div>");
		sb.append("<div class='paramstitle'>Parameters:</div>");
		sb.append("<ul class='paramslist'>");
		for (Parameter param : toolModel.getParameters()) {
			sb.append("<li>");
			sb.append("<span class='paramname'>").append(param.getDisplayName()).append("</span>");
			sb.append(" - ");
			sb.append("<span class='paramdesc'>").append(trim(param.getDescription())).append("</span>");
			sb.append("</li>");
		}
		sb.append("</ul>");
		sb.append("<div class='actionstitle'>Actions:</div>");
		sb.append("<ul class='actionslist'>");
		for (Action a : toolModel.getActions()) {
			sb.append("<li>");
			sb.append("<span class='actionname'>").append(a.getDisplayName()).append("</span>");
			sb.append(" - ");
			sb.append("<span class='actiondesc'>").append(trim(a.getDescription())).append("</span>");
			sb.append("</li>");
		}
		sb.append("</ul>");
		sb.append("</body>");
		return sb.toString();
	}

	private static String trim(String str) {
		return str == null ? "" : str.trim();
	}

}
